module Main where

import Lib
import Player
import Game
import GameState

main :: IO ()
main = do
    n <- getStr
    players <- getPlayers (n :: Int) []
    game <- createGame players
    forever $ do
        game <- playRound game

getPlayers ::Int -> [Player] -> IO [Player]
getPlayers n ps = do
    p <- getPlayer
    if n > 0
        then return getPlayers (n-1) (p:ps)
        else return ps

getPlayer :: IO Player
getPlayer = do
    putStrLn "Player Name?"
    name <- getLine
    putStrLn "Player ID?" -- TODO actual ID gen
    id <- getLine
    return Player name (id :: PlayerId) 1000 []

createGame :: [Player] -> IO GameState
createGame ps = do
    deck <- shuffle52
    return GameState ps deck [] (zip (map playerId ps) [0, 0..])

playRound :: GameState -> GameState
playRound = do
    
