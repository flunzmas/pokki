module HandSpec where

import Test.Hspec

import Card
import Hand

hand_1h2h3h4h5h6h7h = [Card 14 3, Card 2 3, Card 3 3, Card 4 3, Card 5 3, Card 6 3, Card 7 3]
hand_1h2h3h4h5s6s6d = [Card 14 3, Card 2 3, Card 3 3, Card 4 3, Card 5 4, Card 6 4, Card 6 2]
hand_1h2h3h4h5s5c5d = [Card 14 3, Card 2 3, Card 3 3, Card 4 3, Card 5 4, Card 5 1, Card 5 2]
hand_1h2h3h5h5s5d5c = [Card 14 3, Card 2 3, Card 3 3, Card 5 3, Card 5 4, Card 5 2, Card 5 1]
hand_1h1s3h3s5s5d5c = [Card 14 3, Card 14 4, Card 3 3, Card 3 4, Card 5 4, Card 5 2, Card 5 1]
hand_1h1s3h4h5s5d5c = [Card 14 3, Card 14 4, Card 3 3, Card 4 3, Card 5 4, Card 5 2, Card 5 1]
hand_1h1s3h4h5s5d6c = [Card 14 3, Card 14 4, Card 3 3, Card 4 3, Card 5 4, Card 5 2, Card 6 1]
hand_1h1c3h3c5c6c8c = [Card 14 3, Card 14 1, Card 3 3, Card 3 1, Card 5 1, Card 6 1, Card 8 1]
hand_1h1c3hXhJhQhKh = [Card 14 3, Card 14 1, Card 3 3, Card 10 3, Card 11 3, Card 12 3, Card 13 3]
hand_1h1c3h9hJhQhKh = [Card 14 3, Card 14 1, Card 3 3, Card 9 3, Card 11 3, Card 12 3, Card 13 3]

community_3d3s3h5s6s = [Card 3 2, Card 3 4, Card 3 3, Card 5 4, Card 6 4]
community_9sXsJxQsKs = [Card 9 4, Card 10 4, Card 11 4, Card 12 4, Card 13 4]
community_2h3s4d5d7h = [Card 2 3, Card 3 4, Card 4 2, Card 5 2, Card 7 3]

personal_6c9c = (0, [Card 6 1, Card 9 1])
personal_3c2c = (1, [Card 3 1, Card 2 1])
personal_1d1s = (2, [Card 14 2, Card 14 4])
personal_5h5c = (3, [Card 5 3, Card 5 1])
personal_0cJc = (4, [Card 12 1, Card 11 1])
personal_8c9c = (5, [Card 8 1, Card 9 1])
personal_6c8s = (6, [Card 6 1, Card 8 4])
personal_2d6h = (7, [Card 2 2, Card 6 3])
personal_7sXs = (8, [Card 7 4, Card 10 4])

playHighCard = ([personal_8c9c, personal_0cJc], community_2h3s4d5d7h)
playSinglePair = ([personal_7sXs, personal_8c9c, personal_0cJc], community_2h3s4d5d7h)
playDoublePair = ([personal_3c2c, personal_7sXs, personal_8c9c, personal_0cJc], community_2h3s4d5d7h)
playTripleHighCard = ([personal_0cJc, personal_8c9c], community_3d3s3h5s6s)
playStraight = ([personal_8c9c, personal_0cJc, personal_2d6h, personal_6c8s], community_2h3s4d5d7h)
playStraightAnyColor = ([personal_2d6h, personal_6c9c], community_2h3s4d5d7h)
playFlush = ([personal_0cJc, personal_8c9c, personal_7sXs], community_3d3s3h5s6s)
playFullHouse = ([personal_1d1s, personal_6c8s, personal_5h5c], community_3d3s3h5s6s)
playQuads = ([personal_3c2c, personal_1d1s, personal_6c8s, personal_5h5c], community_3d3s3h5s6s)
playStraightFlush = ([personal_5h5c, personal_3c2c, personal_6c8s, personal_8c9c], community_9sXsJxQsKs)
playRoyalFlush = ([personal_5h5c, personal_1d1s, personal_6c8s, personal_8c9c], community_9sXsJxQsKs)

hand_0cJc2h3s4d5d7h = [Card 12 1, Card 11 1, Card 2 3, Card 3 4, Card 4 2, Card 5 2, Card 7 3]
hand_8c9c2h3s4d5d7h = [Card 8 1, Card 9 1, Card 2 3, Card 3 4, Card 4 2, Card 5 2, Card 7 3]
hand_2h3s4d5d7h3c2c = [Card 2 3, Card 3 4, Card 4 2, Card 5 2, Card 7 3, Card 3 1, Card 2 1]

spec :: Spec
spec = do
  describe "hasPairs" $ do
    it (show hand_1h2h3h4h5h6h7h) $
      hasPairs hand_1h2h3h4h5h6h7h `shouldBe` (0, [])

    it (show hand_1h2h3h4h5s6s6d) $
      hasPairs hand_1h2h3h4h5s6s6d `shouldBe` (1, [Card 6 4, Card 6 2, Card 14 3, Card 5 4, Card 4 3])

    it (show hand_1h1s3h4h5s5d5c) $
      hasPairs hand_1h1s3h4h5s5d5c `shouldBe` (2, [Card 14 4, Card 14 3, Card 5 4, Card 5 2, Card 5 1])

    it (show hand_1h2h3h5h5s5d5c) $
      hasPairs hand_1h2h3h5h5s5d5c `shouldBe` (2, [Card 5 4, Card 5 3, Card 5 2, Card 5 1, Card 14 3])
    
  describe "hasTriples" $ do
    it (show hand_1h2h3h4h5h6h7h) $
      hasTriples hand_1h2h3h4h5h6h7h `shouldBe` (False, [])

    it (show hand_1h2h3h4h5s6s6d) $
      hasTriples hand_1h2h3h4h5s6s6d `shouldBe` (False, [])

    it (show hand_1h2h3h4h5s5c5d) $
      hasTriples hand_1h2h3h4h5s5c5d `shouldBe` (True, [Card 5 4, Card 5 2, Card 5 1, Card 14 3, Card 4 3])

  describe "hasQuads" $ do
    it (show hand_1h2h3h4h5h6h7h) $
      hasQuads hand_1h2h3h4h5h6h7h `shouldBe` (False, [])

    it (show hand_1h2h3h4h5s5c5d) $
      hasQuads hand_1h2h3h4h5s5c5d `shouldBe` (False, [])

    it (show hand_1h2h3h5h5s5d5c) $
      hasQuads hand_1h2h3h5h5s5d5c `shouldBe` (True, [Card 5 4, Card 5 3, Card 5 2, Card 5 1, Card 14 3])

  describe "hasStraight" $ do
    it (show hand_1h2h3h4h5s5c5d) $
      hasStraight hand_1h2h3h4h5s5c5d `shouldBe` (True, [Card 5 4, Card 4 3, Card 3 3, Card 2 3, Card 14 3])

    it (show hand_1h2h3h4h5h6h7h) $
      hasStraight hand_1h2h3h4h5h6h7h `shouldBe` (True, [Card 7 3, Card 6 3, Card 5 3, Card 4 3, Card 3 3])

    it (show hand_1h2h3h5h5s5d5c) $
      hasStraight hand_1h2h3h5h5s5d5c `shouldBe` (False, [])

  describe "hasFlush" $ do
    it (show hand_1h2h3h4h5s6s6d) $
      hasFlush hand_1h2h3h4h5s6s6d `shouldBe` (False, [])
      
    it (show hand_1h2h3h4h5h6h7h) $
      hasFlush hand_1h2h3h4h5h6h7h `shouldBe` (True, [Card 14 3, Card 7 3, Card 6 3, Card 5 3, Card 4 3])

    it (show hand_1h1c3h3c5c6c8c) $
      hasFlush hand_1h1c3h3c5c6c8c `shouldBe` (True, [Card 14 1, Card 8 1, Card 6 1, Card 5 1, Card 3 1])

  describe "hasFullHouse" $ do
    it (show hand_1h2h3h4h5s5c5d) $
      hasFullHouse hand_1h2h3h4h5s5c5d `shouldBe` (False, [])

    it (show hand_1h1s3h3s5s5d5c) $
      hasFullHouse hand_1h1s3h3s5s5d5c `shouldBe` (True, [Card 5 4, Card 5 2, Card 5 1, Card 14 4, Card 14 3])
    
    it (show hand_1h1s3h4h5s5d6c) $
      hasFullHouse hand_1h1s3h4h5s5d6c `shouldBe` (False, [])

  describe "hasStraightFlush" $ do
    it (show hand_1h2h3h4h5h6h7h) $ 
      hasStraightFlush hand_1h2h3h4h5h6h7h `shouldBe` (True, [Card 7 3, Card 6 3, Card 5 3, Card 4 3, Card 3 3])

    it (show hand_1h2h3h4h5s5c5d) $
      hasStraightFlush hand_1h2h3h4h5s5c5d `shouldBe` (False, [])

    it (show hand_1h1c3h3c5c6c8c) $
      hasStraightFlush hand_1h1c3h3c5c6c8c `shouldBe` (False, [])

  describe "hasRoyalFlush" $ do
    it (show hand_1h1c3hXhJhQhKh) $
      hasRoyalFlush hand_1h1c3hXhJhQhKh `shouldBe` (True, [Card 14 3, Card 13 3, Card 12 3, Card 11 3, Card 10 3])

    it (show hand_1h1c3h9hJhQhKh) $
      hasRoyalFlush hand_1h1c3h9hJhQhKh `shouldBe` (False, [])

    it (show hand_1h2h3h4h5h6h7h) $ 
      hasRoyalFlush hand_1h2h3h4h5h6h7h `shouldBe` (False, [])

  describe "rankHand" $ do
    it (show hand_0cJc2h3s4d5d7h) $
      rankHand hand_0cJc2h3s4d5d7h `shouldBe` (0, [Card 12 1, Card 11 1, Card 7 3, Card 5 2, Card 4 2])

    it (show hand_8c9c2h3s4d5d7h) $
      rankHand hand_8c9c2h3s4d5d7h `shouldBe` (0, [Card 9 1, Card 8 1, Card 7 3, Card 5 2, Card 4 2])

    it (show hand_2h3s4d5d7h3c2c) $
      rankHand hand_2h3s4d5d7h3c2c `shouldBe` (2, [Card 3 4, Card 3 1, Card 2 3, Card 2 1, Card 7 3])

  describe "getWinners" $ do
    it ("playHighCard: " ++ show playHighCard) $
      uncurry getWinners playHighCard `shouldBe` ([4], [Card 12 1, Card 11 1, Card 7 3, Card 5 2, Card 4 2])

    it ("playSinglePair: " ++ show playSinglePair) $
      uncurry getWinners playSinglePair `shouldBe` ([8], [Card 7 4, Card 7 3, Card 10 4, Card 5 2, Card 4 2])

    it ("playDoublePair: " ++ show playDoublePair) $
      uncurry getWinners playDoublePair `shouldBe` ([1], [Card 3 4, Card 3 1, Card 2 3, Card 2 1, Card 7 3])

    it ("playTripleHighCard: " ++ show playTripleHighCard) $
      uncurry getWinners playTripleHighCard `shouldBe` ([4], [Card 3 4, Card 3 3, Card 3 2, Card 12 1, Card 11 1])

    it ("playStraight: " ++ show playStraight) $
      uncurry getWinners playStraight `shouldBe` ([6], [Card 8 4, Card 7 3, Card 6 1, Card 5 2, Card 4 2])

    it ("playStraightAnyColor: " ++ show playStraightAnyColor) $
      uncurry getWinners playStraightAnyColor `shouldBe` ([0, 7], [Card 7 3, Card 6 3, Card 6 1, Card 5 2, Card 4 2, Card 3 4])

    it ("playFlush: " ++ show playFlush) $
      uncurry getWinners playFlush `shouldBe` ([8], [Card 10 4, Card 7 4, Card 6 4, Card 5 4, Card 3 4])

    it ("playFullHouse: " ++ show playFullHouse) $
      uncurry getWinners playFullHouse `shouldBe` ([3], [Card 5 4, Card 5 3, Card 5 1, Card 3 4, Card 3 3])

    it ("playQuads: " ++ show playQuads) $
      uncurry getWinners playQuads `shouldBe` ([1], [Card 3 4, Card 3 3, Card 3 2, Card 3 1, Card 6 4])

    it ("playStraightFlush: " ++ show playStraightFlush) $
      uncurry getWinners playStraightFlush `shouldBe` ([1, 3, 5, 6], [Card 13 4, Card 12 4, Card 11 4, Card 10 4, Card 9 4])

    it ("playRoyalFlush: " ++ show playRoyalFlush) $
      uncurry getWinners playRoyalFlush `shouldBe` ([2], [Card 14 4, Card 13 4, Card 12 4, Card 11 4, Card 10 4])