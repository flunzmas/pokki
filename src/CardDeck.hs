module CardDeck where

import Card

cardDeck32 = map getCard [(v, c) | v <- [7 .. 14], c <- [1 .. 4]]

cardDeck52 = map getCard [(v, c) | v <- [2 .. 14], c <- [1 .. 4]]
