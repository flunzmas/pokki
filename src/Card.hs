module Card where

import Data.List (sortBy)
import Data.Maybe (fromJust)

valueNames =
  zip
    [1 .. 14]
    ["1", "2", "3", "4", "5", "6", "7", "8", "9", "X", "J", "Q", "K", "A"]

colorNames = zip [1 .. 5] ["♣", "♦", "♥", "♠", "_"]

type CardValue = Int
type CardColor = Int

data Card = Card
  { value :: CardValue
  , color :: CardColor
  } deriving (Eq)

instance Show Card where
  show (Card v c) =
    " " ++
    (fromJust . flip lookup valueNames) v ++
    (fromJust . flip lookup colorNames) c ++ " "

instance Ord Card where
  compare (Card v1 c1) (Card v2 c2)
    | v1 > v2 = GT
    | v1 < v2 = LT
    | otherwise = compare c1 c2

getCard :: (CardValue, CardColor) -> Card
getCard (v, c) = Card v c

dSort :: [Card] -> [Card]
dSort = sortBy $ flip compare

toLowerAce :: Card -> Card
toLowerAce (Card 14 c) = Card 1 c
toLowerAce x = x

toHigherAce :: Card -> Card
toHigherAce (Card 1 c) = Card 14 c
toHigherAce x = x

toAnyColor :: Card -> Card
toAnyColor (Card v c) = Card v 5
