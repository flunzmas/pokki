module Game where 

import Player
import Card
import qualified CardDeck as CD

import System.Random
import Data.Array.ST
import Control.Monad
import Control.Monad.ST
import Data.STRef

shuffle' :: [Card] -> StdGen -> ([Card], StdGen)
shuffle' deck gen = runST (do
                g <- newSTRef gen
                let randomRST lohi = do
                        (a, s') <- fmap (randomR lohi) (readSTRef g)
                        writeSTRef g s'
                        return a
                ar <- newArray n deck
                deck' <- forM [1..n] $ \i -> do
                        j <- randomRST (i, n)
                        vi <- readArray ar i
                        vj <- readArray ar j
                        writeArray ar j vi
                        return vj
                gen' <- readSTRef g
                return (deck', gen'))
    where   n = length deck
            newArray :: Int -> [Card] -> ST s (STArray s Int Card)
            newArray n = newListArray (1, n)
         

shuffleDeck :: [Card] -> IO [Card]
shuffleDeck deck = getStdRandom (shuffle' deck)

shuffle52 :: IO [Card]
shuffle52 = shuffleDeck CD.cardDeck52

-- only to progress the gameState
progressGame :: GameState -> GameState
progressGame GameState ps d c bs| null c        = GameState ps (drop 3 d) (take 3 d) bs  
                                | length c == 3 || length c == 4 = GameState ps (drep 1 d) (d ++ take 1 d) bs 
                                | otherwise     = error "in progressGame"

