-- {-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}

module Hand where

import Data.Function (on)
import Data.List (maximumBy, nub, nubBy, sort, sortBy, sortOn)
import Data.Ord (comparing)

import Cards

type PlayerHand = (Int, [Card])

type HandStrength = (Int, [Card])

instance Ord HandStrength where
  compare (4, cs1) (4, cs2) = compare (map value cs1) (map value cs2) -- color is irrelevant to straights

getWinners :: [(Int, [Card])] -> [Card] -> ([Int], [Card])
getWinners players community =
  getWinners' [(fst p, snd p ++ community) | p <- players]

getWinners' :: [PlayerHand] -> ([Int], [Card])
getWinners' [] = ([], [])
getWinners' [x] = ([fst x], dSort $ snd x)
getWinners' hs = (sort $ map fst winners, nub $ concatMap (snd . snd) winners)
  where
    winners = accumulateWinners (take 1 sortedHands) (drop 1 sortedHands)
    sortedHands =
      sortBy (flip compare `on` snd) $ map (\(id, h) -> (id, rankHand h)) hs

accumulateWinners ::
     [(Int, HandStrength)] -> [(Int, HandStrength)] -> [(Int, HandStrength)]
accumulateWinners ws [] = ws
accumulateWinners ws (x:xs)
  | snd x >= (snd . head) ws = accumulateWinners (x : ws) xs
  | otherwise = ws

rankHand :: [Card] -> HandStrength
rankHand hand
  | length cs /= 7 = error "invalid hand!"
  | fst $ hasRoyalFlush cs = (9, snd $ hasRoyalFlush cs)
  | fst $ hasStraightFlush cs = (8, snd $ hasStraightFlush cs)
  | fst $ hasQuads cs = (7, snd $ hasQuads cs)
  | fst $ hasFullHouse cs = (6, snd $ hasFullHouse cs)
  | fst $ hasFlush cs = (5, snd $ hasFlush cs)
  | fst $ hasStraight cs = (4, snd $ hasStraight cs)
  | fst $ hasTriples cs = (3, snd $ hasTriples cs)
  | fst (hasPairs cs) == 2 = (2, snd $ hasPairs cs)
  | fst (hasPairs cs) == 1 = (1, snd $ hasPairs cs)
  | otherwise = (0, take 5 cs)
  where
    cs = dSort hand

hasRoyalFlush :: [Card] -> (Bool, [Card])
hasRoyalFlush cs
  | fst hsf && value (head $ snd hsf) == 14 = (True, snd hsf)
  | otherwise = (False, [])
  where
    hsf = hasStraightFlush cs

hasStraightFlush :: [Card] -> (Bool, [Card])
hasStraightFlush cs
  | length sf >= 5 = (True, take 5 sf)
  | otherwise = (False, [])
  where
    sf = getStraight $ getFlush cs

hasQuads :: [Card] -> (Bool, [Card])
hasQuads cs
  | not (null quartet) = (True, head quartet ++ take 1 remainingCards)
  | otherwise = (False, [])
  where
    siblings = splitSiblings [] cs
    quartet = map (take 4) $ take 1 $ filter (\l -> length l >= 4) siblings
    remainingCards = dSort $ filter (`notElem` concat quartet) cs

hasFullHouse :: [Card] -> (Bool, [Card])
hasFullHouse cs
  | not (null triplet) && not (null pair) =
    (True, concat triplet ++ concat pair)
  | otherwise = (False, [])
  where
    siblings = splitSiblings [] cs
    triplet = map (take 3) $ take 1 $ filter (\l -> length l >= 3) siblings
    siblings' = splitSiblings [] $ filter (`notElem` concat triplet) cs
    pair = map (take 2) $ take 1 $ filter (\l -> length l >= 2) siblings'

hasFlush :: [Card] -> (Bool, [Card])
hasFlush cs
  | length gf > 4 = (True, take 5 $ dSort gf)
  | otherwise = (False, [])
  where
    gf = getFlush cs

getFlush :: [Card] -> [Card]
getFlush cs =
  maximumBy (comparing length) [filter (\x -> color x == i) cs | i <- [1 .. 4]]

hasStraight :: [Card] -> (Bool, [Card])
hasStraight cs
  | length gs >= 5 = (True, take 5 gs)
  | length ss >= 5 = (True, take 5 ss)
  | otherwise = (False, [])
  where
    gs = nubBy (\x y -> value x == value y) $ getStraight cs
    ss =
      nubBy (\x y -> value x == value y) $
      map toHigherAce $ getStraight $ map toLowerAce cs

getStraight :: [Card] -> [Card]
getStraight cs =
  dSort $ maximumBy (comparing length) $ splitStraights [] $ dSort cs

hasTriples :: [Card] -> (Bool, [Card])
hasTriples cs
  | not (null triplet) = (True, head triplet ++ take 2 remainingCards)
  | otherwise = (False, [])
  where
    siblings = splitSiblings [] cs
    triplet = map (take 3) $ take 1 $ filter (\l -> length l >= 3) siblings
    remainingCards = dSort $ filter (`notElem` concat triplet) cs

hasPairs :: [Card] -> (Int, [Card])
hasPairs cs
  | length pairs == 2 = (2, concat pairs ++ take 1 remainingCards)
  | length pairs == 1 = (1, head pairs ++ take 3 remainingCards)
  | otherwise = (0, [])
  where
    siblings = splitSiblings [] cs
    pairs =
      take 2 $
      filter (\l -> length l == 2) $
      concatMap (\l -> [take 2 l, drop 2 l]) siblings
    remainingCards = dSort $ filter (`notElem` concat pairs) cs

-- Helpers
splitStraights :: [[Card]] -> [Card] -> [[Card]]
splitStraights strs [] = strs
splitStraights [] (c:cs) = splitStraights [[c]] cs
splitStraights (s:strs) (c:cs)
  | value (head s) <= (value c + 1) = splitStraights ((c : s) : strs) cs
  | otherwise = splitStraights ([c] : s : strs) cs

splitSiblings :: [[Card]] -> [Card] -> [[Card]]
splitSiblings sibs [] = reverse $ map dSort sibs
splitSiblings [] (c:cs) = splitSiblings [[c]] cs
splitSiblings (s:sibs) (c:cs)
  | value (head s) == value c = splitSiblings ((c : s) : sibs) cs
  | otherwise = splitSiblings ([c] : s : sibs) cs
