module Player where

import Card
import Data.Maybe
import Data.List (sortBy)
import Data.Function (on)

type PlayerId = Int

data Player = Player
  { playerID :: PlayerId
  , playerName :: String
  , chipCount :: Int
  , currentHand :: [Card]
  }

instance Eq Player where
  p1 == p2 = ((==) `on` playerID) p1 p2

instance Ord Player where
  compare = compare `on` chipCount

instance Show Player where
  show (Player i n c []) =
    "Player " ++ show n ++ " (ID: " ++ show i ++ "): " ++ show c ++ " chips"
  show (Player i n c h) = show (Player i n c []) ++ ". Hand: " ++ show h

rankPlayers :: [Player] -> [Player]
rankPlayers = sortBy (flip compare)

hasWinner :: [Player] -> Maybe Player
hasWinner [] = Nothing
hasWinner [p] = Just p
hasWinner ps
  | chipCount leader > 0 && all (\p -> chipCount p <= 0) rest = Just leader
  | otherwise = Nothing
  where
    (leader, rest) = (head rankedPlayers, tail rankedPlayers)
    rankedPlayers = rankPlayers ps

addChips :: Int -> Player -> Player
addChips amount (Player i n c h) = Player i n (c + amount) h

subtractChips :: Int -> Player -> Player
subtractChips amount = addChips (-amount)

assignHand :: [Card] -> Player -> Player
assignHand hand (Player i n c _) = Player i n c hand

yieldHand :: Player -> Player
yieldHand = assignHand []