module GameState where 


import Player
import Card
import CardDeck

data GameState = GameState
    {  
       players :: [Player],
       shuffledDeck :: [Card],
       community :: [Card],
       playerBets :: [(PlayerId, Bool, Int)]
    }